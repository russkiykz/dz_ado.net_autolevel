﻿using System;
using System.Data;

namespace HomeworkAutoLevel
{
    class Program
    {
        static void Main(string[] args)
        {

            DataSet shopDB = new DataSet("ShopDB");

            DataTable orders = new DataTable("Orders");

            DataColumn ordersColumnId = new DataColumn("ID",typeof(Guid));
            DataColumn ordersColumnCustomersId = new DataColumn("customersId",typeof(Guid));
            DataColumn ordersColumnEmployeesId = new DataColumn("employeesId", typeof(Guid));
            DataColumn ordersColumnOrderDetailsId = new DataColumn("orderDetailsId", typeof(Guid));

            orders.Columns.AddRange(new DataColumn[] { ordersColumnId, ordersColumnCustomersId, ordersColumnEmployeesId, ordersColumnOrderDetailsId});

            DataTable customers = new DataTable("Customers");

            DataColumn customersColumnId = new DataColumn("ID", typeof(Guid));
            DataColumn customersColumnName = new DataColumn("name", typeof(string));
            customersColumnName.AllowDBNull=false;

            customers.Columns.AddRange(new DataColumn[] { customersColumnId, customersColumnName });

            DataTable employees = new DataTable("Employees");

            DataColumn employeesColumnId = new DataColumn("ID", typeof(Guid));
            DataColumn employeesColumnName = new DataColumn("name", typeof(string));
            employeesColumnName.AllowDBNull = false;
            DataColumn employeesColumnPosition = new DataColumn("position", typeof(string));
            employeesColumnPosition.AllowDBNull = false;
            DataColumn employeesColumnWorkExperience = new DataColumn("workExperience", typeof(int));

            employees.Columns.AddRange(new DataColumn[] {employeesColumnId,employeesColumnName,employeesColumnPosition,employeesColumnWorkExperience });

            DataTable orderDetails = new DataTable("OrderDetails");

            DataColumn orderDetailsColumnId = new DataColumn("ID", typeof(Guid));
            DataColumn orderDetailsColumnName = new DataColumn("name", typeof(string));
            orderDetailsColumnName.AllowDBNull = false;
            DataColumn orderDetailsColumnDate = new DataColumn("date", typeof(DateTime));
            DataColumn orderDetailsColumnProductsId = new DataColumn("productsId", typeof(Guid));
            DataColumn orderDetailsColumnPrice = new DataColumn("price", typeof(int));
            orderDetailsColumnPrice.AllowDBNull = false;

            orderDetails.Columns.AddRange(new DataColumn[] { orderDetailsColumnId, orderDetailsColumnName,orderDetailsColumnDate,orderDetailsColumnProductsId,orderDetailsColumnPrice });

            DataTable products = new DataTable("Products");

            DataColumn productsColumnId = new DataColumn("ID", typeof(Guid));
            DataColumn productsColumnName = new DataColumn("name", typeof(string));
            productsColumnName.AllowDBNull = false;

            products.Columns.AddRange(new DataColumn[] { productsColumnId, productsColumnName });


            orders.PrimaryKey = new DataColumn[] { orders.Columns["ID"] };
            customers.PrimaryKey = new DataColumn[] { customers.Columns["ID"] };
            employees.PrimaryKey = new DataColumn[] { employees.Columns["ID"] };
            orderDetails.PrimaryKey = new DataColumn[] { orderDetails.Columns["ID"] };
            products.PrimaryKey = new DataColumn[] { products.Columns["ID"] };

            ForeignKeyConstraint fkOrdersCustomers = new ForeignKeyConstraint("FK_Orders_Customers", customers.Columns["ID"],orders.Columns["customersId"]);
            ForeignKeyConstraint fkOrdersEmployees = new ForeignKeyConstraint("FK_Orders_Employees", employees.Columns["ID"],orders.Columns["employeesId"]);
            ForeignKeyConstraint fkOrdersOrderDetails = new ForeignKeyConstraint("FK_Orders_OrderDetails", orderDetails.Columns["ID"], orders.Columns["orderDetailsId"]);
            ForeignKeyConstraint fkOrderDetailsProducts = new ForeignKeyConstraint("FK_OrderDetails_Products", products.Columns["ID"],orderDetails.Columns["productsId"]);

            orders.Constraints.AddRange(new Constraint[] { fkOrdersCustomers, fkOrdersEmployees, fkOrdersOrderDetails });
            orderDetails.Constraints.Add(fkOrderDetailsProducts);

            shopDB.Tables.AddRange(new DataTable[] { orders, customers,employees,orderDetails,products });
        }
    }
}
